;;; spongebob.el --- Memetically mock a region of text

;; Copyright (C) 2019  Christine Lemmer-Webber <cwebber@dustycloud.org>
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; To use, add spongebob.el to your emacs lisp path and add the following
;; to your ~/.emacs or ~/.emacs.d/init.el
;;
;; (require 'spongebob)
;;
;; Then highlight a region of text and run:
;;   M-x spongebobify-text

(require 'studly)

;; I guess Spongebob is copyright and trademark of Viacom, even though
;; the ascii art is my work.  Claiming fair use here!

(defvar spongebob-ascii
  "              .--.
              \\ +\\\\
          .~._.\\V \\\\._.~._.~._
         (      \\==)          /)
        (~._.~.~/ /~._.~._.~./  (
        \\ \\ | |'-'      \\ | /(   )
         (.---.         .---. )  (   //|
         ((o)  )       (  (o))(   ).'_.||.///
         _'-_-'/        '---'  )  :  / ||;' )
        (_____.' '         -.-  ( '-'.'--'.-'
         /   _-----.             )  o ) \\ \\
        ;  .') _____\\        __   (   ( |/
        : / '-'             (__)   )_/ .'
         V    '--.               o   ~_/
                 '----_______-------'
                 /   /      /   /
                 '--'       '--'
                 //  _      //  _
                 _\\\\/ \\     _\\\\/ \\
                (    ,/    (    ,/
                 '--'       '--'")

(defun spongebob-center-text (text)
  (with-temp-buffer
    (insert text)
    (let ((fill-column 50))
      (fill-region (point-min) (point-max))
      (center-region (point-min) (point-max)))
    (buffer-string)))

(defun spongebob-format-words ()
  (let ((text (buffer-substring (mark) (point))))
    (with-temp-buffer
      (insert text)
      (insert "\n")
      (goto-char (point-min))
      (studlify-region (point-min) (point-max))
      (let* ((num-words (count-words (point-min) (point-max)))
             (half-words (floor num-words 2)))
        ;; Now go forward that many words
        (forward-word half-words)
        (let ((first-half (buffer-substring (point-min) (point)))
              (second-half (buffer-substring (+ (point) 1) (point-max))))
          (cons (spongebob-center-text first-half)
                (spongebob-center-text second-half)))))))

(defun spongebobify-region ()
  "Memetically mock a region of text."
  (interactive)
  (let* ((words (spongebob-format-words)))
    (delete-region (mark) (point))
    (insert (car words))
    (insert "\n\n")
    (insert spongebob-ascii)
    (insert "\n\n")
    (insert (cdr words))))

(provide 'spongebob)
